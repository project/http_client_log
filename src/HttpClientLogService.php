<?php

namespace Drupal\http_client_log;

use Drupal\Core\Http\ClientFactory;
use Drupal\http_client_log\Logger\Logger;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\TransferStats;
use GuzzleLogMiddleware\Handler\HandlerInterface;
use GuzzleLogMiddleware\LogMiddleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Define HttpClientLogService class.
 */
class HttpClientLogService extends ClientFactory {

  /**
   * {@inheritdoc}
   */
  public function __construct(HandlerStack $stack) {
    parent::__construct($stack);
    $logger = new Logger();
    $this->stack->push(new LogMiddleware($logger, new SimpleHandler()));
  }

}

/**
 * A simple handler that logs only requests and response .*/
final class SimpleHandler implements HandlerInterface {

  /**
   * {@inheritdoc}
   */
  public function log(
    LoggerInterface $logger,
    RequestInterface $request,
    ?ResponseInterface $response = NULL,
    ?\Throwable $exception = NULL,
    ?TransferStats $stats = NULL,
    array $options = [],
  ): void {
    $context['request'] = $request;
    $context['response'] = $response;
    $context['options'] = $options;
    $logger->log(NULL, NULL, $context);
  }

}
