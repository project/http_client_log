<?php

namespace Drupal\http_client_log\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines HttpClientLogEntityTypeForm class.
 */
class HttpClientLogEntityTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $http_client_log_entity_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $http_client_log_entity_type->label(),
      '#description' => $this->t("Label for the Http client log entity type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $http_client_log_entity_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\http_client_log\Entity\HttpClientLogEntityType::load',
      ],
      '#disabled' => !$http_client_log_entity_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $http_client_log_entity_type = $this->entity;
    $status = $http_client_log_entity_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Http client log entity type.', [
          '%label' => $http_client_log_entity_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Http client log entity type.', [
          '%label' => $http_client_log_entity_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($http_client_log_entity_type->toUrl('collection'));
  }

}
