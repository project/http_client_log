<?php

namespace Drupal\http_client_log\Logger;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Logger\RfcLoggerTrait;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;

/**
 * Defines Logger class.
 */
class Logger implements LoggerInterface {

  use RfcLoggerTrait;
  use DependencySerializationTrait;

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []) :void {
    /** @var \GuzzleHttp\Psr7\Request $request */
    $request = $context['request'];
    /** @var \GuzzleHttp\Psr7\Response $response */
    $response = $context['response'];
    $response_headers = [];
    $content_type = '';
    try {
      foreach ($request->getHeaders() as $req_k => $req_v) {
        $request_headers[] = $req_k . ': ' . implode('|', $req_v);
      }
      if ($response instanceof Response) {
        foreach ($response->getHeaders() as $res_k => $res_v) {
          $response_headers[] = $res_k . ': ' . implode('|', $res_v);
        }
        if ($response->hasHeader('Content-Type')) {
          $content_type = $response->getHeader('Content-Type')[0];
        }
      }
      // Only logs the html, xml and json, like application/json.
      if (str_contains($content_type, 'text/html') || str_contains($content_type, 'json') || str_contains($content_type, 'application/xml')) {
        // See discussion here:
        // https://github.com/guzzle/guzzle/pull/1262#issuecomment-149080749
        $response_body = $response->getBody()->getContents();
        $requestPos = $request->getBody()->tell();
        $responsePos = $response->getBody()->tell();
        if ($request->getBody()->isSeekable() && $requestPos != $request->getBody()->tell()) {
          $request->getBody()->rewind();
        }
        if ($response->getBody()->isSeekable() && $responsePos != $response->getBody()->tell()) {
          $response->getBody()->rewind();
        }
        $entity = \Drupal::entityTypeManager()
          ->getStorage('http_client_log')
          ->create([
            'type' => 'http_client_log',
            'changed' => \Drupal::time()->getCurrentTime(),
            'request_http_method' => $request->getMethod(),
            'request_url' => $request->getUri(),
            'request_headers' => implode("\r\n", $request_headers),
            'request_payload' => $request->getBody(),
            'response_status_code' => $response->getStatusCode(),
            'response_reason_phrase' => $response->getReasonPhrase(),
            'response_headers' => implode("\r\n", $response_headers),
            'response_body' => $response_body,
            'context' => $context,
          ]);
        $response->getBody()->rewind();

        $entity->save();
      }
    }
    catch (RequestException $e) {
      $entity = \Drupal::entityTypeManager()
        ->getStorage('http_client_log')
        ->create([
          'type' => 'http_client_log',
          'request_http_method' => $request->getMethod(),
          'request_url' => $request->getUri(),
          'request_payload' => $request->getBody(),
          'response_status_code' => $response->getStatusCode(),
          'response_reason_phrase' => $response->getReasonPhrase(),
          'errors' => $e->getMessage(),
          'context' => $context,
        ]);
      $entity->save();
    }
    catch (BadResponseException $e) {
      $entity = \Drupal::entityTypeManager()
        ->getStorage('http_client_log')
        ->create([
          'type' => 'http_client_log',
          'request_http_method' => $request->getMethod(),
          'request_url' => $request->getUri(),
          'request_payload' => $request->getBody(),
          'response_status_code' => $response->getStatusCode(),
          'response_reason_phrase' => $response->getReasonPhrase(),
          'errors' => $e->getMessage(),
          'context' => $context,
        ]);
      $entity->save();
    }
  }

}
