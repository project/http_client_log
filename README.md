# HTTP Client Log Module

The HTTP Client Log module logs outgoing HTTP requests made by Drupal using the Guzzle HTTP client.

## Installation

1. Download the module and place it in your Drupal modules directory.
2. Enable the module on the Extend page (/admin/modules).

## Configuration

There are no configuration options for this module. Once enabled, it will start logging outgoing HTTP requests.

## Usage

Once the module is enabled, you can view the logged HTTP requests in the Drupal logs.

```
use Drupal\Component\Serialization\Json;
$base_url = "https://httpbin.org";
$client = \Drupal::httpClient();

$response = $client->get($base_url . '/get');
$data = Json::Decode($response->getBody());
dpm($data);

$response = $client->get($base_url . '/anything', [
  'headers' => [
    'accept' => 'application/json'
  ],
]);
$data = Json::decode($response->getBody());
dpm($data);

$response = $client->post($base_url . '/status/500', [
  'verify' => true,
  'headers' => [
    'accept' => 'text/plain'
  ],
]);
$data = $response->getBody()->getContents();
dpm($data);
```
