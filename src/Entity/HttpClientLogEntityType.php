<?php

namespace Drupal\http_client_log\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Http client log entity type entity.
 *
 * @ConfigEntityType(
 *   id = "http_client_log_entity_type",
 *   label = @Translation("Http client log entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\http_client_log\HttpClientLogEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\http_client_log\Form\HttpClientLogEntityTypeForm",
 *       "edit" = "Drupal\http_client_log\Form\HttpClientLogEntityTypeForm",
 *       "delete" = "Drupal\http_client_log\Form\HttpClientLogEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\http_client_log\HttpClientLogEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "http_client_log_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "http_client_log",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/http_client_log_entity_type/{http_client_log_entity_type}",
 *     "add-form" = "/admin/structure/http_client_log_entity_type/add",
 *     "edit-form" = "/admin/structure/http_client_log_entity_type/{http_client_log_entity_type}/edit",
 *     "delete-form" = "/admin/structure/http_client_log_entity_type/{http_client_log_entity_type}/delete",
 *     "collection" = "/admin/structure/http_client_log_entity_type"
 *   }
 * )
 */
class HttpClientLogEntityType extends ConfigEntityBundleBase implements HttpClientLogEntityTypeInterface {

  /**
   * The Http client log entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Http client log entity type label.
   *
   * @var string
   */
  protected $label;

}
