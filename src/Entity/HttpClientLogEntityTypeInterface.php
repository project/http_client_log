<?php

namespace Drupal\http_client_log\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Http client log entity type entities.
 */
interface HttpClientLogEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
