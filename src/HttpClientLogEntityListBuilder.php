<?php

namespace Drupal\http_client_log;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Http client log entity entities.
 *
 * @ingroup http_client_log
 */
class HttpClientLogEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Http client log entity ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\http_client_log\Entity\HttpClientLogEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.http_client_log_entity.edit_form',
      ['http_client_log_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
