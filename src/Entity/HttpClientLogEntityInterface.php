<?php

namespace Drupal\http_client_log\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Http client log entity entities.
 *
 * @ingroup http_client_log
 */
interface HttpClientLogEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Http client log entity name.
   *
   * @return string
   *   Name of the Http client log entity.
   */
  public function getName();

  /**
   * Sets the Http client log entity name.
   *
   * @param string $name
   *   The Http client log entity name.
   *
   * @return \Drupal\http_client_log\Entity\HttpClientLogEntityInterface
   *   The called Http client log entity entity.
   */
  public function setName($name);

  /**
   * Gets the Http client log entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Http client log entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Http client log entity creation timestamp.
   *
   * @param int $timestamp
   *   The Http client log entity creation timestamp.
   *
   * @return \Drupal\http_client_log\Entity\HttpClientLogEntityInterface
   *   The called Http client log entity entity.
   */
  public function setCreatedTime($timestamp);

}
